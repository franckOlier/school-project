#include "fdf.h"

void	set_iso_point(t_env *e, t_iso_point *p, int x, int y)
{
	p->iso_x = x;
	p->iso_y = y;
}

void	set_iso_point_z(t_env *e, t_iso_point *p, int z)
{
	int		ix;
	int		iy;

	p->iso_z = z;
	ix = p->iso_x;
	iy = p->iso_y;
	p->td_x = ((ix - iy) * e->ratio) + e->origin_x;
	p->td_y = ((ix * e->ratio + iy * e->ratio) / 2) - z + e->origin_y;
}

void	draw_iso_line(t_env *e, t_iso_point p1, t_iso_point p2)
{
	draw_line(e, p1, p2);
}
