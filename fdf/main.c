#include "fdf.h"

void	draw(t_env *e)
{
	print_all(e, e->first);
}

void	close_on_file_error(void)
{
	write(2, "error reading file\n", 19);
	exit(0);
}

void	zero_init(int *v1, char *v2, int *v3)
{
	*v1 = 0;
	*v2 = 0;
	*v3 = 0;
}

int		get_best_ratio(t_env *e)
{
	t_matrix	*cpy;
	int			sum_cols;
	int			sum_lines;

	cpy = *e->first;
	sum_cols = cpy->c_l_vals;
	sum_lines = cpy->line_max;
	if (sum_cols >= sum_lines)
		return ((1000 / sum_cols) + 1);
	else
		return ((1000 / sum_lines) + 1);
}

int		main(int argc, char **argv)
{
	t_env		e;
	t_matrix	*first;

	first = NULL;
	if (argc == 2)
	{
		parse_file(&e, argv[1], &first);
		set_window_size(&e, 2400, 1200);
		set_iso_size(&e, 1200, 20, get_best_ratio(&e));
		if (e.mlx != NULL)
		{
			e.win = mlx_new_window(e.mlx, e.total_x, e.total_y, "FdF");
			mlx_key_hook(e.win, key_hook, &e);
			mlx_expose_hook(e.win, expose_hook, &e);
			mlx_loop(e.mlx);
		}
		else
			write(2, "Error initializing mlx\n", 23);
	}
	else
		write(2, "Only one file input.\n", 21);
	return (0);
}
