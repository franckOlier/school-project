#include "fdf.h"

int				get_draw_order(int x_s, int x_end, int y_s, int y_end)
{
	int				abs_x;
	int				abs_y;

	abs_x = ft_abs(x_end - x_s);
	abs_y = ft_abs(y_end - y_s);
	if (abs_x > abs_y)
		return (1);
	else
		return (2);
}

t_line_coord	*create_line_s(int nx_s, int nx_e, int ny_s, int ny_e)
{
	t_line_coord	*coords;

	coords = (t_line_coord *)malloc(sizeof(t_line_coord));
	coords->x_start = nx_s;
	coords->x_end = nx_e;
	coords->y_start = ny_s;
	coords->y_end = ny_e;
	coords->draw_order = get_draw_order(nx_s, nx_e, ny_s, ny_e);
	if (coords->draw_order == 1)
	{
		coords->longest = ft_abs(nx_e - nx_s);
		coords->shortest = ft_abs(ny_e - ny_s);
	}
	else
	{
		coords->longest = ft_abs(ny_e - ny_s);
		coords->shortest = ft_abs(nx_e - nx_s);
	}
	coords->dir_x = (nx_e < nx_s) ? -1 : 1;
	coords->dir_y = (ny_e < ny_s) ? -1 : 1;
	return (coords);
}

void			draw_line(t_env *e, t_iso_point p1, t_iso_point p2)
{
	t_line_coord	*coord;
	int				i;
	int				current_abs;
	int				current_x;
	int				current_y;

	coord = create_line_s(p1.td_x, p2.td_x, p1.td_y, p2.td_y);
	i = 0;
	while (i <= coord->longest)
	{
		current_abs = (i * coord->shortest) / (coord->longest);
		if (coord->draw_order == 1)
		{
			current_x = coord->x_start + (i * coord->dir_x);
			current_y = coord->y_start + (current_abs * coord->dir_y);
			mlx_pixel_put(e->mlx, e->win, current_x, current_y, 0x00FF00);
		}
		else
		{
			current_x = coord->x_start + (current_abs * coord->dir_x);
			current_y = coord->y_start + (i * coord->dir_y);
			mlx_pixel_put(e->mlx, e->win, current_x, current_y, 0xFF0000);
		}
		i++;
	}
}
