#include "fdf.h"

int		count_max(char *line)
{
	int			current;
	int			i;
	int			len;

	current = 0;
	i = 0;
	len = ft_strlen(line);
	while (i < len)
	{
		if (ft_isdigit(line[i]) == 1)
		{
			if (i == (len - 1))
				current++;
			else if (ft_isdigit(line[i + 1]) == 0)
				current++;
		}
		i++;
	}
	return (current);
}

void	add_line_max(t_matrix **first, int lm)
{
	t_matrix	*browser;

	browser = *first;
	while (browser)
	{
		browser->line_max = lm;
		browser = browser->next;
	}
}

void	set_next_iso(t_env *e, t_matrix *next, t_iso_point *point)
{
	set_iso_point(e, point, next->curr_val_i, next->curr_line);
	set_iso_point_z(e, point, next->value);
}

int		set_nl_iso(t_env *e, t_matrix **next, t_iso_point *point)
{
	t_matrix	*browser;
	int			cvi;
	int			cl;

	browser = *next;
	cvi = browser->curr_val_i;
	cl = browser->curr_line;
	browser = browser->next;
	while (browser->curr_val_i != cvi
		&& (browser->curr_line == cl || browser->curr_line == (cl + 1)))
		browser = browser->next;
	if (browser->curr_val_i == cvi)
	{
		set_iso_point(e, point, browser->curr_val_i, browser->curr_line);
		set_iso_point_z(e, point, browser->value);
		return (1);
	}
	return (0);
}

void	print_all(t_env *e, t_matrix **first)
{
	t_matrix	*browser;
	t_iso_point	*p0;
	t_iso_point	*p1;
	t_iso_point	*p2;

	browser = *first;
	while (browser)
	{
		p0 = (t_iso_point *)malloc(sizeof(t_iso_point));
		p1 = (t_iso_point *)malloc(sizeof(t_iso_point));
		p2 = (t_iso_point *)malloc(sizeof(t_iso_point));
		set_iso_point(e, p0, browser->curr_val_i, browser->curr_line);
		set_iso_point_z(e, p0, browser->value);
		if (browser->curr_val_i < (browser->c_l_vals - 1))
		{
			set_next_iso(e, browser->next, p1);
			draw_iso_line(e, *p0, *p1);
		}
		if (browser->curr_line < (browser->line_max - 1))
		{
			if (set_nl_iso(e, &browser, p2))
				draw_iso_line(e, *p0, *p2);
		}
		browser = browser->next;
	}
}
