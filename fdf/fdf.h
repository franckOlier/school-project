#ifndef FDF_H
# define FDF_H
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <sys/uio.h>
# include <sys/types.h>
# include <mlx.h>
# include <fcntl.h>
# include <stdio.h>
# define BUFF_SIZE 128

typedef struct		s_env
{
	struct s_matrix **first;
	void			*mlx;
	void			*win;
	int				total_x;
	int				total_y;
	int				origin_x;
	int				origin_y;
	int				ratio;
	int				sum_values;
}					t_env;

typedef struct		s_line_coord
{
	int				x_start;
	int				x_end;
	int				y_start;
	int				y_end;
	int				draw_order;
	int				longest;
	int				shortest;
	int				dir_x;
	int				dir_y;
}					t_line_coord;

typedef struct		s_iso_point
{
	int				iso_x;
	int				iso_y;
	int				iso_z;
	int				td_x;
	int				td_y;
}					t_iso_point;

typedef struct		s_point_2d
{
	int				td_x;
	int				td_y;
}					t_point_2d;

typedef struct		s_matrix
{
	int				value;
	int				curr_val_i;
	int				curr_line;
	int				c_l_vals;
	int				line_max;
	struct s_matrix	*next;
}					t_matrix;

/*
** main.c
*/

void				draw(t_env *e);
void				close_on_file_error(void);
void				zero_init(int *v1, char *v2, int *v3);
void				parse_file(t_env *e, char *line_addr, t_matrix **first);

/*
** draw_iso.c
*/

void				set_iso_point(t_env *e, t_iso_point *p, int x, int y);
void				set_iso_point_z(t_env *e, t_iso_point *p, int z);
void				draw_iso_line(t_env *e, t_iso_point p1, t_iso_point p2);

/*
** draw_line.c
*/

int					get_draw_order(int x_s, int x_end, int y_s, int y_end);
t_line_coord		*create_line_s(int nx_s, int nx_e, int ny_s, int ny_e);
void				draw_line(t_env *e, t_iso_point p1, t_iso_point p2);

/*
** gnl.c
*/

void				malloc_line(char **line, char *inter);
int					get_next_line(int const fd, char **line);

/*
** gnl_ft.c
*/

char				*ft_strnew(size_t size);
char				*ft_strncpy(char *s1, const char *s2, size_t n);
void				ft_bzero(void *s, size_t n);
char				*ft_strdup(const char *s1);
char				*ft_strchr(const char *s, int c);

/*
** gnl_ft2.c
*/

char				*ft_strjoin(char const *s1, char const *s2);
size_t				ft_strlen(char const *s);
void				ft_putendl(char const *s);
void				ft_putchar(char c);
void				*ft_memset(void *b, int c, size_t len);

/*
** gnl_ft3.c
*/

void				ft_putstr(const char *str);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
int					ft_strcmp(const char *s1, const char *s2);

/*
** math.c
*/

int					ft_abs(int value);
int					ft_max(int value_a, int value_b);
int					ft_isdigit(int c);
int					ft_atoi(const char *str);

/*
** mlx_hooks.c
*/

int					expose_hook(t_env *e);
int					key_hook(int keycode, t_env *e);
void				set_window_size(t_env *e, int s_x, int s_y);
void				set_iso_size(t_env *e, int s_x, int s_y, int r);

/*
** parser.c
*/

int					count_max(char *line);
void				add_line_max(t_matrix **first, int lm);
void				set_next_iso(t_env *e, t_matrix *next, t_iso_point *point);
int					set_nl_iso(t_env *e, t_matrix **next, t_iso_point *point);
void				print_all(t_env *e, t_matrix **first);

/*
** parser2.c
*/

void				add_line_matrix(t_matrix **first, int cl, int cvi, int v);
void				add_clvals(t_matrix **first, int cl, int clv);
void				split_line_values(t_matrix **first, int cl, char *line);
void				parse_file(t_env *e, char *file_addr, t_matrix **first);

#endif
