#include "fdf.h"

int		ft_abs(int value)
{
	if (value < 0)
		return (-value);
	else
		return (value);
}

int		ft_max(int value_a, int value_b)
{
	if (value_a >= value_b)
		return (value_a);
	else
		return (value_b);
}

int		ft_isdigit(int c)
{
	if (c >= 48 && (c <= 57 || c == 45))
		return (1);
	else
		return (0);
}

int		ft_atoi(const char *str)
{
	int		i;
	int		compt;
	int		min;

	i = 0;
	compt = 0;
	min = 0;
	while (str[i] == ' ' || (str[i] >= 8 && str[i] <= 14))
		i++;
	if (str[i] == '-')
	{
		i++;
		min++;
	}
	else if (str[i] == '+')
		i++;
	while (ft_isdigit(str[i]))
	{
		compt = compt * 10;
		compt = compt + str[i] - '0';
		i++;
	}
	if (min > 0)
		return (-compt);
	return (compt);
}
