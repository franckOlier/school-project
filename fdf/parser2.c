#include "fdf.h"

void	add_line_matrix(t_matrix **first, int cl, int cvi, int v)
{
	t_matrix	*n_i;
	t_matrix	*browser;

	n_i = (t_matrix *)malloc(sizeof(t_matrix));
	n_i->value = v;
	n_i->curr_line = cl;
	n_i->curr_val_i = cvi;
	if (*first == NULL)
	{
		*first = n_i;
		browser = *first;
	}
	else
	{
		browser = *first;
		while (browser->next != NULL)
			browser = browser->next;
	}
	browser->next = n_i;
	n_i->next = NULL;
}

void	add_clvals(t_matrix **first, int cl, int clv)
{
	t_matrix	*browser;

	browser = *first;
	while (browser)
	{
		if (browser->curr_line == cl)
			browser->c_l_vals = clv;
		browser = browser->next;
	}
}

void	split_line_values(t_matrix **first, int cl, char *line)
{
	char		current_pos;
	int			i;
	char		value[12];
	int			value_i;

	ft_bzero(value, 12);
	zero_init(&value_i, &current_pos, &i);
	while (i <= ft_strlen(line))
	{
		if ((ft_isdigit(line[i]) == 0 && current_pos > 0)
			|| (i == ft_strlen(line) && current_pos > 0))
		{
			add_line_matrix(first, cl, value_i, ft_atoi(value));
			ft_bzero(value, 12);
			current_pos = 0;
			value_i++;
		}
		else if (ft_isdigit(line[i]) == 1)
		{
			value[current_pos] = line[i];
			current_pos++;
		}
		i++;
	}
	add_clvals(first, cl, value_i);
}

void	count_all_values(t_env *e)
{
	t_matrix	*cpy;
	int			count;

	cpy = *e->first;
	count = 0;
	while (cpy != NULL)
	{
		count++;
		cpy = cpy->next;
	}
	e->sum_values = count;
}

void	parse_file(t_env *e, char *file_addr, t_matrix **first)
{
	char		*line;
	int			fd;
	int			count;

	e->mlx = mlx_init();
	e->first = first;
	fd = open(file_addr, O_RDONLY);
	if (fd == -1)
		close_on_file_error();
	else
	{
		count = 0;
		while (get_next_line(fd, &line))
		{
			split_line_values(first, count, line);
			count++;
		}
		close(fd);
		add_line_max(first, count);
	}
	count_all_values(e);
}
