#include "fdf.h"

int		expose_hook(t_env *e)
{
	draw(e);
	return (0);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == 65307)
		exit(0);
	return (0);
}

void	set_window_size(t_env *e, int t_x, int t_y)
{
	e->total_x = t_x;
	e->total_y = t_y;
}

void	set_iso_size(t_env *e, int s_x, int s_y, int r)
{
	e->origin_x = s_x;
	e->origin_y = s_y;
	e->ratio = r;
}
