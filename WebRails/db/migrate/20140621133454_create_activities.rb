class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.date :start
      t.date :end
      t.date :registration_start
      t.date :registration_end
      t.text :users
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
