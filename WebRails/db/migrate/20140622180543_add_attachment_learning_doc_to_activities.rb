class AddAttachmentLearningDocToActivities < ActiveRecord::Migration
  def self.up
    change_table :activities do |t|
      t.attachment :learning_doc
    end
  end

  def self.down
    drop_attached_file :activities, :learning_doc
  end
end
