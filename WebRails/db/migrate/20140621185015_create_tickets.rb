class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :title
      t.text :description
      t.integer :priority
      t.boolean :fixed
      t.integer :user_id
      t.integer :assignee_id

      t.timestamps
    end
  end
end
