class AddBirthDateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :'birth-date', :string
  end
end
