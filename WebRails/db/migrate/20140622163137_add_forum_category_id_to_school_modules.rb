class AddForumCategoryIdToSchoolModules < ActiveRecord::Migration
  def change
    add_column :school_modules, :forum_category_id, :integer
  end
end
