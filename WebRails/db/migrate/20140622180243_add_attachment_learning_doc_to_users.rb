class AddAttachmentLearningDocToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :learning_doc
    end
  end

  def self.down
    drop_attached_file :users, :learning_doc
  end
end
