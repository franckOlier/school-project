class AddSchoolModuleIdToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :school_module_id, :integer
  end
end
