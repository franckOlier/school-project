class AddForumIdToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :forum_id, :integer
  end
end
