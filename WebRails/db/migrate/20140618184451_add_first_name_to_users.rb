class AddFirstNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :'first-name', :string
  end
end
