class AddMobilePhoneToUsers < ActiveRecord::Migration
  def change
    add_column :users, :'mobile-phone', :string
  end
end
