class AddNameAndDescriptionToSchoolModules < ActiveRecord::Migration
  def change
    add_column :school_modules, :name, :string
    add_column :school_modules, :description, :text
  end
end
