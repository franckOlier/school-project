class CreateControllers < ActiveRecord::Migration
  def change
    create_table :controllers do |t|
      t.string :school_modules

      t.timestamps
    end
  end
end
