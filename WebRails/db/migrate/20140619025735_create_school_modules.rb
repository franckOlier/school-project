class CreateSchoolModules < ActiveRecord::Migration
  def change
    create_table :school_modules do |t|
      t.date :start
      t.date :end
      t.date :registration_start
      t.date :registration_end
      t.text :users

      t.timestamps
    end
  end
end
