class ProfilesController < ApplicationController
  def index
    @page = (params[:page] || 1).to_i
    sort_key = params[:sort_by] || :username
    @users = User.order(sort_key).page(@page).per(48)
  end

  def show
    @user = User.where(username: params[:id]).first
    unless @user
      flash[:alert] = t(:no_user_found)
      redirect_to root_path
    end
  end

  @@lock_sync = false

  def sync
    if !@@lock_sync
      Thread.new do
        @@lock_sync = true;
        User.sync
        @@lock_sync = false;
      end
    end
    @lock_sync = @@lock_sync
  end
end
