class TicketsController < ApplicationController
  def index
    @tickets = Ticket.all
  end

  def new
    @ticket = Ticket.new
  end

  def show
    @ticket = Ticket.find params[:id]
  end

  def create
    Ticket.create(set_ticket_params)
    respond_to do |format|
      format.html {redirect_to :back}
    end
  end

  def set_ticket_params
    params.require(:ticket).permit(:title, :description, :priority, :fixed, :assignee, :submit)
  end
end
