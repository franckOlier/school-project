class UsersController < ApplicationController
	def disable
		u = User.find(params[:id])
		u.autologindestroy
		flash[:notice] = "Autologin disabled"
		redirect_to :back
	end
	def generate
		u = User.find(params[:id])
		u.autologincreate
		flash[:notice] = "Autologin token : " + u.token
		redirect_to :back
	end
end
