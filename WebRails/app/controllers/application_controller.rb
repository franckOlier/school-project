class ApplicationController < ActionController::Base
  helper_method :forem_user
  before_action :set_locale

  def set_locale
    I18n.locale = current_user[:locale] rescue :en
  end

  def forem_user
    current_user
  end

  def forem_email
    current_user.email
  end

  rescue_from DeviseLdapAuthenticatable::LdapException do |exception|
    render :text => exception, :status => 500
  end
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!


  def authenticate_user_from_token!
	if params[:token].presence
		if params[:token].empty?
			redirect_to root_url
			return
		end
		username = params[:user].presence
		user = username && User.where(username: params[:user]).first
		if user
			if !(user.token.empty?)
				if Devise.secure_compare(user.token, params[:token])
					sign_in :user, user
					redirect_to root_url
				end
			else
				redirect_to root_url
			end
		end
	end
  end

  def can_read_forem_category?(category) true end
  def can_read_forem_forums?() true end
  def can_read_forem_forum?(forum) true end
  def can_create_forem_topics?(forum) true end
  def can_read_forem_topic?(topic) true end
  def can_reply_to_forem_topic?(topic) true end
  def can_edit_forem_posts?(forum) true end

  def change_locale
    current_user.change_locale(params[:new_locale])
    redirect_to :back
  end
end
