module ProfilesHelper
  def image_of(user)
    user[:picture] ? "data:image/jpg;base64,#{Base64.encode64(user[:picture])}" : "nopicture-profilview.png"
  end
end
