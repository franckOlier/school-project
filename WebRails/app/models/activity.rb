class Activity < ActiveRecord::Base
  serialize  :users, Array
  belongs_to :school_module
  before_save :create_forum
  # has_one :forum, class_name: 'Forem::Forum', foreign_key: 'forum_id'
  has_attached_file :learning_doc, :default_url => "/learning_doc_default.pdf"
  do_not_validate_attachment_file_type :learning_doc
  validates :school_module_id, :presence => true
  validates :name, :presence => true, :length => { minimum: 3, maximum: 32 }

  def register(username)
    self.users << username unless self.users.include?(username)
    self.save
    self
  end

  def unregister(username)
    self.users.delete_if { |u| u == username}
    self.save
    self
  end

  def create_forum
    unless Forem::Forum.where(name: self.name).first
      f = Forem::Forum.new(name: self.name,
                           category_id: self.school_module.forum_category.id,
                           description: self.description)
      f.save
      self.forum_id = f.id
    end
  end
  
  def title
    self.name
  end
end
