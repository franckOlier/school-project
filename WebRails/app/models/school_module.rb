class SchoolModule < ActiveRecord::Base
  serialize :users, Array
  before_save :create_forum
  validates :name, :presence => true, :length => { minimum: 3, maximum: 32 }
  validates :start, :presence => true
  validates :end, :presence => true
  validates :registration_start, :presence => true
  validates :registration_end, :presence => true

  def register(username)
    self.users << username unless self.users.include?(username)
    self.save
    self
  end

  def unregister(username)
    self.users.delete_if { |u| u == username}
    self.save
    self
  end

  def create_forum
    unless Forem::Category.where(name: self.name).first
      f = Forem::Category.new(name: self.name)
      f.save
      self.forum_category_id = f.id
    end
  end

  def forum_category
    Forem::Category.where(id: forum_category_id).first
  end
  alias :forem_category :forum_category
end
