class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
devise :ldap_authenticatable, :trackable
#  require 'securerandom'

  def forem_user
    self
  end

  def forem_email
    self[:email]
  end

  before_save :get_ldap_data

  def birth_date_time
    bd = self[:'birth-date']
    month = bd.slice(4..5)
    day = bd.slice(6..7)
    year = bd.slice(0..3)
    "#{day}/#{month}/#{year}"
  end

  def get_ldap_data
    to_get = [
      :'cn',
      :'uidnumber',
      :'other',
      :'gidnumber',
      :'first-name',
      :'last-name',
      :'mobile-phone',
      :'birth-date',
      :'picture',
    ]
    entry = Devise::LDAP::Adapter.get_ldap_entry(self.username)
    to_get.each do |k|
      self[k] = entry[k].first
    end
    self[:email] = entry[:alias][1 % entry[:alias].count]
  end

  def self.sync
    remote = Devise::LDAP::Connection.new.ldap.search(attributes: [:'uid'])
    remote.each_slice(8) do |batch|
      ActiveRecord::Base.transaction do
        batch.each do |entry|
          uid = entry[:uid][0].to_s #DRY
          user = User.where(username: uid).first rescue nil
          user.save!(username: uid) rescue next if user #Si l'user existe on update
          User.create!(username: uid) rescue next unless user #Sinon on create
        end
      end
    end
  end

  def autologincreate
  	self.token = SecureRandom.hex
  	self.save
  end

  def autologindestroy
  	self.token = ''
  	self.save
  end

  def to_s
    self.username
  end

  def school_modules(options = {})
    SchoolModule.all.select do |m|
      m.users.include?(self.username)
    end
  end

  def activities(options = {upcoming: false, old: false})
    Activity.all.select do |a|
      begin
        if options[:upcoming] == true && !a.users.include?(self.username) && Date.today <= a.start
          true
        elsif options[:upcoming] == false && a.users.include?(self.username) && Date.today <= a.end && Date.today >= a.start
          true
        elsif options[:old] == true && a.users.include?(self.username) && Date.today > a.end
          true
        end
      rescue
        true
      end
    end
  end

  def forem_admin
    true
  end

  def can_read_forem_category?(category) true end
  def can_read_forem_forums?() true end
  def can_read_forem_forum?(forum) true end
  def can_create_forem_topics?(forum) true end
  def can_read_forem_topic?(topic) true end
  def can_reply_to_forem_topic?(topic) true end
  def can_edit_forem_posts?(forum) true end

  def change_locale(new_locale)
    self[:locale] = new_locale
    self.save
  end

  def Object.attachment_definitions
  end
end
