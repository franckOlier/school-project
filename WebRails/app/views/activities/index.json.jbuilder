json.array!(@activities) do |activity|
  json.extract! activity, :id, :name, :description
  json.start_time activity.start_time
  json.end_time activity.end_time
  json.url activity_url(event, format: :html)
end
