json.array!(@activity) do |activity|
  json.extract! activity, :id, :title, :description, :start, :end
  json.url activity_url(activity, format: :json)
end
