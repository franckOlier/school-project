json.array!(@school_modules) do |school_module|
  json.extract! school_module, :id, :start, :end, :registration_start, :registration_end, :users
  json.url school_module_url(school_module, format: :json)
end
