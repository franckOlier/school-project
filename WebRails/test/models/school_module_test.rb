require 'test_helper'

class SchoolModuleTest < ActiveSupport::TestCase
  test "should register user nmohamed in Algo" do
    m = SchoolModule.first
    m.register("nmohamed")
    assert m.users.include?('nmohamed')
  end

  test "should find user qwert in Algo" do
    m = SchoolModule.first
    assert m.users.include?('qwert')
  end

  test "should NOT find user lolilol in Algo" do
    m = SchoolModule.first
    assert m.users.include?('lolilol') == false
  end
end
