require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should find user nmohamed" do
    assert User.where(username: 'nmohamed').first.username == "nmohamed", "should return nmohamed"
  end

  test "should return bgauci" do
    assert User.where(username: 'bgauci').first.username != 'nmohamed', "should return bgauci"
  end
end
