#include "fractol.h"

int			assign_renderer_2(t_env *e, char *renderer_name)
{
	if (ft_strcmp(renderer_name, "mandelbrot") == 0)
	{
		e->render_init = &(prepare_render_mandelbrot);
		e->render_run = &(render_mandelbrot);
	}
	else if (ft_strcmp(renderer_name, "chou") == 0)
	{
		e->render_init = &(prepare_render_chou);
		e->render_run = &(render_chou);
	}
	else
		return (0);
	return (1);
}

int			assign_renderer(t_env *e, char *renderer_name)
{
	int		retval;

	retval = 0;
	if (ft_strcmp(renderer_name, "julia_1") == 0)
	{
		e->render_init = &(prepare_render_julia);
		e->render_run = &(render_julia_1);
		retval = 1;
	}
	else if (ft_strcmp(renderer_name, "julia_2") == 0)
	{
		e->render_init = &(prepare_render_julia);
		e->render_run = &(render_julia_2);
		retval = 1;
	}
	else
		retval = assign_renderer_2(e, renderer_name);
	return (retval);
}
