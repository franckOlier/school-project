#include "fractol.h"

t_render	*prepare_render_chou(t_env *e)
{
	t_render	*render;

	render = (t_render *)malloc(sizeof(t_render));
	e->screen_x = 600;
	e->screen_y = 600;
	render->x_a = -1.2;
	render->x_b = 1.2;
	render->y_a = -1.2;
	render->y_b = 1.4;
	render->current_x_section = 4;
	render->current_y_section = 4;
	render->it_max = 400;
	return (render);
}

int			loop_chou(t_env *e, double params[7])
{
	int		i;
	int		color;
	int		max_render;

	i = 0;
	max_render = e->render->it_max;
	params[5] = params[2] * params[2];
	params[6] = params[3] * params[3];
	while (params[5] + params[6] < 4.0 && i < max_render)
	{
		params[4] = params[2];
		params[2] = params[5] - params[6] + params[0];
		params[4] += params[4];
		params[3] = params[4] * params[3] + params[1];
		params[5] = params[2] * params[2];
		params[6] = params[3] * params[3];
		i++;
	}
	color = color_wawesome(i % 360, 2.95, 0.5 * (i < e->render->it_max));
	color = mlx_get_color_value(e->mlx, color);
	return (color);
}

void		render_chou(t_env *e, t_render *render)
{
	int		curr_x;
	int		curr_y;
	double	params[5];
	int		color;

	render->zoom_x = e->screen_x / (render->x_b - render->x_a);
	render->zoom_y = e->screen_y / (render->x_b - render->x_a);
	curr_x = 0;
	while (curr_x < e->screen_x)
	{
		curr_y = 0;
		while (curr_y < e->screen_y)
		{
			params[0] = 0.285 + 0.001 * render->current_x_section;
			params[1] = 0.01 + 0.0001 * render->current_y_section;
			params[2] = (double)curr_x / (double)render->zoom_x + render->x_a;
			params[3] = (double)curr_y / (double)render->zoom_y + render->y_a;
			color = loop_chou(e, &params[0]);
			put_pixel_to_image(e, color, curr_x, curr_y);
			curr_y++;
		}
		curr_x++;
	}
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
}
