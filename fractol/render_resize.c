#include "fractol.h"

void	new_x_size(t_env *e, double c_x, int mode)
{
	double	new_tmp_a;
	double	new_tmp_b;

	new_tmp_a = c_x - ((e->render->x_b - e->render->x_a) / mode);
	new_tmp_b = c_x + ((e->render->x_b - e->render->x_a) / mode);
	e->render->x_a = new_tmp_a;
	e->render->x_b = new_tmp_b;
}

void	new_y_size(t_env *e, double c_y, int mode)
{
	double	new_tmp_a;
	double	new_tmp_b;

	new_tmp_a = c_y - ((e->render->y_b - e->render->y_a) / mode);
	new_tmp_b = c_y + ((e->render->y_b - e->render->y_a) / mode);
	e->render->y_a = new_tmp_a;
	e->render->y_b = new_tmp_b;
}

int		mouse_move_hook(int x, int y, t_env *e)
{
	int		z;
	int		w;

	z = (int)(((float)x / (float)e->screen_x) * 5);
	w = (int)(((float)y / (float)e->screen_y) * 5);
	if (z != e->render->current_x_section || w != e->render->current_y_section)
	{
		if (e->allow_variations == 1 && z >= 0 && z < 5 && w >= 0 && w < 5)
		{
			e->render->current_x_section = z;
			e->render->current_y_section = w;
			e->render_run(e, e->render);
		}
	}
	return (0);
}
