#include "fractol.h"

int		expose_hook(t_env *e)
{
	e->render_run(e, e->render);
	return (0);
}

void	put_pixel_to_image(t_env *e, unsigned long img_color, int x, int y)
{
	e->img_data[y * e->mlx_sl + x * (e->mlx_bpp / 8) + 2] = img_color >> 16;
	e->img_data[y * e->mlx_sl + x * (e->mlx_bpp / 8) + 1] = img_color >> 8;
	e->img_data[y * e->mlx_sl + x * (e->mlx_bpp / 8) + 0] = img_color >> 0;
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == 53)
		exit(0);
	else if (keycode == 258)
	{
		if (e->allow_variations == 0)
			e->allow_variations = 1;
		else
			e->allow_variations = 0;
		return (0);
	}
	e->render_run(e, e->render);
	return (0);
}

int		mouse_click_hook(int button, int x, int y, t_env *e)
{
	t_render	*r;
	double		c_x;
	double		c_y;

	r = e->render;
	c_x = r->x_a + (((double)x / (double)e->screen_x) * (r->x_b - r->x_a));
	c_y = r->y_a + (((double)y / (double)e->screen_y) * (r->y_b - r->y_a));
	if (button == 4)
	{
		new_x_size(e, c_x, 4);
		new_y_size(e, c_y, 4);
		e->render_run(e, e->render);
	}
	else if (button == 5)
	{
		new_x_size(e, c_x, 1);
		new_y_size(e, c_y, 1);
		e->render_run(e, r);
	}
	return (0);
}

void	set_window_size(t_env *e)
{
	e->screen_x = WIN_X;
	e->screen_y = WIN_Y;
}
