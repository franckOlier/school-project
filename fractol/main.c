#include "fractol.h"

int			ft_strcmp(const char *p1, const char *p2)
{
	const unsigned char *s1;
	const unsigned char *s2;
	unsigned char		c1;
	unsigned char		c2;

	s1 = (const unsigned char *)p1;
	s2 = (const unsigned char *)p2;
	c1 = (unsigned char)*s1++;
	c2 = (unsigned char)*s2++;
	if (c1 == '\0')
		return (c1 - c2);
	while (c1 == c2)
	{
		c1 = (unsigned char)*s1++;
		c2 = (unsigned char)*s2++;
		if (c1 == '\0')
			return (c1 - c2);
	}
	return (c1 - c2);
}

int			color_wawesome(int h, float s, float l)
{
	int		color;
	float	c;
	float	x;
	float	m;

	c = (1.0 - fabs(2 * l - 1.0)) * s;
	x = c * (1.0 - fabs(fmod((h / 60.0), 2) - 1.0));
	m = l - 0.5 * c;
	if (h >= 0 && h < 360)
		return (((c + m) * 255 * 256 * 256 + (x + m) * 255 * 256 + m * 255));
	color = 0;
	return (color);
}

void		start_renderer(t_env *e, char *argv)
{
	e->render = e->render_init(e);
	e->win = mlx_new_window(e->mlx, e->screen_x, e->screen_y, argv);
	e->img = mlx_new_image(e->mlx, e->screen_x, e->screen_y);
	e->img_data = mlx_get_data_addr(e->img, &e->mlx_bpp, &e->mlx_sl, &e->mlx_e);
	e->allow_variations = 1;
	e->render_run(e, e->render);
	mlx_expose_hook(e->win, expose_hook, e);
	mlx_key_hook(e->win, key_hook, e);
	mlx_mouse_hook(e->win, mouse_click_hook, e);
	mlx_hook(e->win, 6, 1L << 6, mouse_move_hook, e);
	mlx_loop(e->mlx);
}

int			main(int argc, char **argv)
{
	t_env		*e;

	e = (t_env *)malloc(sizeof(t_env));
	if (argc >= 2)
	{
		write(2, "Controls:\n", 10);
		write(2, " - mouse position for render variations.\n", 41);
		write(2, " - mouse scroll for zoom in/out.\n", 33);
		write(2, " - ctrl for locking/unlocking variations.\n", 42);
		set_window_size(e);
		e->mlx = mlx_init();
		if (e->mlx != NULL)
		{
			if (assign_renderer(e, argv[1]) == 1)
				start_renderer(e, argv[1]);
			else
				write(2, "Options: julia_1 or 2, mandelbrot, chou.\n", 41);
		}
		else
			write(2, "Error initializing mlx.\n", 24);
	}
	else
		write(2, "Options: julia_1 or 2, mandelbrot, chou.\n", 41);
	return (0);
}
