#ifndef FRACTOL_H
# define FRACTOL_H
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <sys/uio.h>
# include <sys/types.h>
# include <mlx.h>
# include <fcntl.h>
# include <stdio.h>
# include <math.h>
# define WIN_X		1000
# define WIN_Y		500
# define BASE_ZOOM	100
# define UL			unsigned long

typedef struct		s_render
{
	double			x_a;
	double			x_b;
	double			y_a;
	double			y_b;
	int				current_x_section;
	int				current_y_section;
	unsigned int	zoom_x;
	unsigned int	zoom_y;
	int				it_max;
}					t_render;

typedef struct		s_env
{
	t_render		*render;
	char			*img_data;
	void			*img;
	void			*win;
	void			*mlx;
	int				screen_x;
	int				screen_y;
	int				zoom;
	int				mlx_bpp;
	int				mlx_sl;
	int				mlx_e;
	int				allow_variations;
	t_render		*(*render_init)(struct s_env *);
	void			(*render_run)(struct s_env *, t_render *);
}					t_env;

/*
** main.c
*/

int					ft_strcmp(const char *p1, const char *p2);
int					color_wawesome(int h, float s, float l);
void				start_renderer(t_env *e, char *argv);
int					main(int argc, char **argv);

/*
** mlx.c
*/

void				put_pixel_to_image(t_env *e, UL img_color, int x, int y);
int					expose_hook(t_env *e);
int					key_hook(int keycode, t_env *e);
void				set_window_size(t_env *e);
int					mouse_click_hook(int button, int x, int y, t_env *e);

/*
** workaround_mlx.c
*/

t_env				*set_get_envir(t_env *e_in, int set);
void				set_envir(t_env *e);
t_env				*get_envir();

/*
** julia.c
*/

t_render			*prepare_render_julia(t_env *e);
int					julia_loop(t_env *e, double params[5]);
void				render_julia_1(t_env *e, t_render *render);
void				render_julia_2(t_env *e, t_render *render);
void				render_julia_3(t_env *e, t_render *render);

/*
** mandelbrot.c
*/

t_render			*prepare_render_mandelbrot(t_env *e);
int					loop_mandelbrot(t_env *e, double params[5]);
void				render_mandelbrot(t_env *e, t_render *render);

/*
** render_resize.c
*/

void				new_x_size(t_env *e, double c_x, int mode);
void				new_y_size(t_env *e, double c_y, int mode);
int					mouse_move_hook(int x, int y, t_env *e);

/*
** render_assign.c
*/

int					assign_renderer_2(t_env *e, char *renderer_name);
int					assign_renderer(t_env *e, char *renderer_name);

/*
** tree.c
*/

t_render			*prepare_render_chou(t_env *e);
int					loop_chou(t_env *e, double params[7]);
void				render_chou(t_env *e, t_render *render);

#endif
