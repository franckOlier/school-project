/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heart_wolf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/17 06:40:30 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 20:16:11 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#define RAYDIR ray->ray_dir
#define RAYPOS ray->ray_pos
#define E_RAYDIR e->ray->ray_dir
#define E_RAYPOS e->ray->ray_pos
#define RAYDIST ray->side_dist
#define RAYLN e->ray->ln_ray
#define RAY e->ray

static void		size_wall(double ray_ln, t_wall *wall)
{
	int		size_wall;

	size_wall = abs(WIN_HEIGHT / ray_ln);
	wall->draw_start = (-size_wall / 2) + (WIN_HEIGHT / 2);
	if (wall->draw_start < 0)
		wall->draw_start = 0;
	wall->draw_end = (size_wall / 2) + (WIN_HEIGHT / 2);
	if (wall->draw_end > WIN_HEIGHT)
		wall->draw_end = WIN_HEIGHT - 1;
}

static void		calc_ln(t_calc *ray)
{
	if (ray->ray_dir[X] < 0)
	{
		ray->step[X] = -1;
		RAYDIST[X] = (RAYPOS[X] - ray->map[X]) * ray->delta_dist[X];
	}
	else
	{
		ray->step[X] = 1;
		RAYDIST[X] = (ray->map[X] + 1.0 - RAYPOS[X]) * ray->delta_dist[X];
	}
	if (ray->ray_dir[Y] < 0)
	{
		ray->step[Y] = -1;
		RAYDIST[Y] = (RAYPOS[Y] - ray->map[Y]) * ray->delta_dist[Y];
	}
	else
	{
		ray->step[Y] = 1;
		RAYDIST[Y] = (ray->map[Y] + 1.0 - RAYPOS[Y]) * ray->delta_dist[Y];
	}
}

static t_calc	*calc_ray(t_env *e, int x_win)
{
	e->ray->cam_x = ((2 * x_win) / (double)WIN_WIDTH - 1);
	E_RAYPOS[X] = e->init->pos[X];
	E_RAYPOS[Y] = e->init->pos[Y];
	E_RAYDIR[X] = e->init->dir[X] + (e->init->plane[X] * e->ray->cam_x);
	E_RAYDIR[Y] = e->init->dir[Y] + (e->init->plane[Y] * e->ray->cam_x);
	e->ray->map[X] = (int)E_RAYPOS[X];
	e->ray->map[Y] = (int)E_RAYPOS[Y];
	e->ray->delta_dist[X]
		= sqrt(1 + ((pow(E_RAYDIR[Y], 2.0) / pow(E_RAYDIR[X], 2.0))));
	e->ray->delta_dist[Y]
		= sqrt(1 + ((pow(E_RAYDIR[X], 2.0) / pow(E_RAYDIR[Y], 2.0))));
	e->ray->status[HIT] = 0;
	calc_ln(e->ray);
	return (e->ray);
}

static void		dda_ln(t_calc *ray, t_tab *map_y)
{
	if (RAYDIST[X] < RAYDIST[Y])
	{
		RAYDIST[X] += ray->delta_dist[X];
		ray->map[X] += ray->step[X];
		ray->status[SIDE] = 0;
	}
	else
	{
		RAYDIST[Y] += ray->delta_dist[Y];
		ray->map[Y] += ray->step[Y];
		ray->status[SIDE] = 1;
	}
	if (map_y[ray->map[Y]].map_x[ray->map[X]] > 0)
		ray->status[HIT] = 1;
}

int				heart_wolf(t_env *e)
{
	int			x;

	x = 0;
	while (x < WIN_WIDTH)
	{
		RAY = calc_ray(e, x);
		while (RAY->status[HIT] == 0)
			dda_ln(RAY, e->init->map_y);
		if (RAY->status[SIDE] == 0)
			RAYLN = fabs((RAY->map[X] - E_RAYPOS[X]
						+ (1 - RAY->step[X]) / 2) / E_RAYDIR[X]);
		else
			RAYLN = fabs((RAY->map[Y] - E_RAYPOS[Y]
						+ (1 - RAY->step[Y]) / 2) / E_RAYDIR[Y]);
		size_wall(RAYLN, e->wall);
		draw_column_img(e->wall, e->pix, x, e);
		x++;
	}
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	return (0);
}
