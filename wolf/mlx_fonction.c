/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_fonction.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/16 06:14:59 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 20:45:51 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

#define ESC 65307
#define BEFORE 65362
#define BACK 65364
#define LEFT 65361
#define RIGHT 65363

static t_tab_fonc	*key_fonc(void)
{
	static t_tab_fonc	fonc[] =			{
		{ESC,		(t_fonc) esc_fonc		},
		{BEFORE,	(t_fonc) before_fonc	},
		{BACK,		(t_fonc) back_fonc		},
		{LEFT,		(t_fonc) left_fonc		},
		{RIGHT,		(t_fonc) right_fonc		},
		{0,		NULL						}
	};
	return (fonc);
}

int		expose_hook(t_env *win)
{
	return (0);
}

int		key_hook(int key_code, t_env *win)
{
	int			i;
	t_tab_fonc	*fonc;

	fonc = key_fonc();
	i = 0;
	while (fonc[i].keycode != 0)
	{
		if (key_code == fonc[i].keycode)
			return (fonc[i].fonc(win));
		i++;
	}
	return (0);
}
