/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 06:48:19 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 12:58:06 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include <math.h>
# include <mlx.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include "libft/include/libft.h"

# define WIN_HEIGHT 384
# define WIN_WIDTH 512
# define WIN_NAME "Wolf3d by 42"
# define X 0
# define Y 1
# define HIT 0
# define SIDE 1

typedef struct		s_wall
{
	int				draw_start;
	int				draw_end;
}					t_wall;

typedef struct		s_img
{
	char			*img;
	int				bpp;
	int				szln;
	int				end;
	unsigned char	red;
	unsigned char	green;
	unsigned char	blue;
}					t_img;

typedef struct	s_tab
{
	int			map_x[24];
}				t_tab;

typedef struct	s_calc
{
	double		cam_x;
	double		ray_pos[2];
	double		ray_dir[2];
	int			map[2];
	double		side_dist[2];
	double		delta_dist[2];
	double		ln_ray;
	int			step[2];
	int			status[2];
}				t_calc;

typedef struct	s_time
{
	double		ftime;
	double		mvspeed;
	double		rotspeed;
}				t_time;

typedef struct	s_data
{
	t_tab		*map_y;
	double		pos[2];
	double		dir[2];
	double		plane[2];
	t_time		*time;
}				t_data;

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	void			*img;
	t_wall			*wall;
	t_img			*pix;
	t_data			*init;
	t_calc			*ray;
}					t_env;

typedef int (*t_fonc)(t_env *);

typedef struct	s_tab_fonc
{
	int			keycode;
	t_fonc		fonc;
}				t_tab_fonc;

typedef const struct	s_error
{
	const int			size;
	const char			*error;
}						t_error;

/*
**		WOLF_MAIN
*/

/*
**		WOLF_IMG
*/

void	draw_column_img(t_wall *wall, t_img *pix, int x, t_env *e);

/*
**		HEART_WOLF
*/

int		heart_wolf(t_env *e);

/*
**		WOLF_ERROR
*/

int		wolf_error(int error, char *txt_error);


/*
**		KEY_FONCTION
*/

int		esc_fonc(t_env *win);
int		before_fonc(t_env *win);
int		back_fonc(t_env *win);
int		left_fonc(t_env *win);
int		right_fonc(t_env *win);

/*
**		MLX_FONCTION
*/

int		expose_hook(t_env *win);
int		key_hook(int key_code, t_env *win);

/*
**		WOLF_MAP
*/

t_tab	*world_one(void);

/*
**		WOLF_INIT
*/

void	init_win(t_env *win);

#endif /* !WOLF3D_H */
