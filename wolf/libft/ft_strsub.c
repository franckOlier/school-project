/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:20:04 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 12:55:03 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"

char				*ft_strsub(const char *s1, unsigned int start, size_t len)
{
	char			*str;
	unsigned int	cnt;

	if (!s1)
		return (NULL);
	cnt = start + len;
	str = (char *)malloc((len + 1) * (sizeof(char)));
	if (!str)
		return (NULL);
	while (cnt > start)
	{
		*str = s1[start];
		start++;
		str++;
	}
	*str = '\0';
	return (str - len);
}
