/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 17:53:35 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:16:27 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "include/libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	int		c1;
	int		c2;

	c1 = 0;
	c2 = 0;
	while (s1[c1] && n > 0)
	{
		while (s1[c1] == s2[c2] && n > 0)
		{
			c2++;
			c1++;
			n--;
		}
		if (s2[c2] == '\0')
			return ((char *)(s1 + c1 - c2));
		else
		{
			c1 = c1 + 1 - c2;
			n = n - 1 + c2;
			c2 = 0;
		}
	}
	return (NULL);
}
