/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstnew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/01 19:29:52 by folier            #+#    #+#             */
/*   Updated: 2014/01/16 21:20:51 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"

t_dlist		*ft_dlstnew(void *content, size_t content_size)
{
	t_dlist		*new;

	if ((new = malloc(sizeof(t_dlist))) == NULL)
		return (NULL);
	new->next = NULL;
	new->prev = NULL;
	if (content == NULL)
	{
		new->content = NULL;
		new->content_size = 0;
		return (new);
	}
	new->content = ft_memalloc(content_size + 1);
	new->content = ft_memcpy(new->content, content, content_size);
	new->content_size = content_size;
	return (new);
}
