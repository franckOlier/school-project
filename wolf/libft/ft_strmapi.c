/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:15:19 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:15:50 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

char	*ft_strmapi(const char *s, char (*f)(unsigned int, char))
{
	char			*dest;
	unsigned int	cnt;

	if (!s || !f)
		return (NULL);
	cnt = 0;
	dest = ft_strnew(ft_strlen(s));
	while (s[cnt])
	{
		dest[cnt] = f(cnt, s[cnt]);
		cnt++;
	}
	return (dest);
}
