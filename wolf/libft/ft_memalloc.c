/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:04:22 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:12:51 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"

void	*ft_memalloc(size_t size)
{
	unsigned char	*str;

	str = malloc(size*sizeof(char));
	if (!str)
		return (NULL);
	else
		ft_bzero(str, size);
	return (void *)(str);
}
