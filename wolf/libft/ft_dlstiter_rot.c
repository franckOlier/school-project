/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstiter_rot.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/05 13:38:46 by folier            #+#    #+#             */
/*   Updated: 2014/01/05 22:25:55 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

void	ft_dlstiter_rot(t_dlist *dlst, void (*f)(t_dlist *elem))
{
	t_dlist		*tmp;

	tmp = dlst->next;
	f(dlst);
	while (tmp != dlst)
	{
		f(tmp);
		tmp = tmp->next;
	}
}
