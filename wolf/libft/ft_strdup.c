/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 10:40:37 by folier            #+#    #+#             */
/*   Updated: 2014/01/16 21:23:13 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>
#include "include/libft.h"

char	*ft_strdup(const char *s1)
{
	char	*clone;
	size_t	count;

	count = 0;
	clone = (char*)malloc((ft_strlen(s1) + 1) * sizeof(char));
	if (!clone)
		return (NULL);
	while (s1[count])
	{
		clone[count] = s1[count];
		count++;
	}
	clone[count] = '\0';
	return (clone);
}
