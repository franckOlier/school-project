/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 17:36:16 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 12:56:17 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# define BUFF_SIZE 1
# define MAX_FD_NBR 10
# include <string.h>

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct		s_dlist
{
	void			*content;
	size_t			content_size;
	struct s_dlist	*next;
	struct s_dlist	*prev;
}					t_dlist;

typedef struct		s_tree
{
	void			*content;
	size_t			content_size;
	struct s_tree	*root;
	struct s_tree	*next;
	struct s_tree	*prev;
}					t_tree;

/*
**		ft_put
*/

void	ft_putchar(char c);
void	ft_putchar_fd(char c, int fd);
void	ft_putendl(char const *s);
void	ft_putendl_fd(char const *s, int fd);
void	ft_putstr(char const *s);
void	ft_putstr_fd(char const *s, int fd);
void	ft_putnbr(int n);
void	ft_putnbr_fd(int n, int fd);

/*
**		ft_mem
*/

void	*ft_memset(void *b, int c, size_t len);
void	ft_bzero(void *s, size_t n);
void	*ft_memcpy(void *s1, const void *s2, size_t n);
void	*ft_memccpy(void *s1, const void *s2, int c, size_t n);
void	*ft_memmove(void *s1, const void *s2, size_t n);
void	*ft_memcat(void *s1, void *s2, int len1, int len2);
void	*ft_memchr(const void *s, int c, size_t n);
int		ft_memcmp(const void *s1, const void *s2, size_t n);
void	*ft_memalloc(size_t size);
void	ft_memdel(void **ap);

/*
**		STR
*/

size_t	ft_strlen(const char *s);
char	*ft_strcut(const char *str, size_t cut);
char	*ft_strcpy(char *s1, const char *s2);
char	*ft_strncpy(char *s1, const char *s2, size_t n);
char	*ft_strdup(const char *s1);
char	**ft_str2dup(char **str);
char	*ft_strcat(char *s1, const char *s2);
char	*ft_strncat(char *s1, const char *s2, size_t n);
size_t	ft_strlcat(char *dst, const char *src, size_t size);
char	*ft_strchr(const char *s, int c);
char	*ft_strrchr(const char *s, int c);
char	*ft_strstr(const char *s1, const char *s2);
char	*ft_strnstr(const char *s1, const char *s2, size_t n);
int		ft_strcmp(const char *s1, const char *s2);
int		ft_strncmp(const char *s1, const char *s2, size_t n);
char	*ft_strnew(size_t size);
char	**ft_strsplit(char const *s, char c);
char	**ft_strsplit_ac(char const *s, char c, int *ac);
char	*ft_strjoin(char const *s1, char const *s2);
char	*ft_free_strjoin(char *s1, char *s2, const int op);
void	ft_str2del(char ***as);
void	ft_strdel(char **as);
void	ft_strclr(char *s);
void	ft_striter(char *s, void (*f)(char *));
void	ft_striteri(char *s, void (*f)(unsigned int, char *));
char	*ft_strmap(char const *s, char (*f)(char));
char	*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int		ft_strequ(char const *s1, char const *s2);
int		ft_strnequ(char const *s1, char const *s2, size_t n);
char	*ft_strsub(char const *s, unsigned int start, size_t len);
char	*ft_strtrim(char const *s);

/*
**		ft_check_char
*/

int		ft_isalpha(int c);
int		ft_isdigit(int c);
int		ft_isalnum(int c);
int		ft_isascii(int c);
int		ft_isprint(int c);
int		ft_toupper(int c);
int		ft_tolower(int c);

/*
**		ft_divers
*/

int		ft_atoi(const char *str);
char	*ft_itoa(int n);
int		ft_get_next_line(int const fd, char **line);
int		ft_free_return(char *str, const int value);

/*
**		ft_list
*/

t_list	*ft_lstnew(void const *content, size_t content_size);
void	ft_lstadd(t_list **alst, t_list *new);
void	ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void	ft_lstdel(t_list **alst, void (*del)(void *, size_t));

/*
**		ft_dlist
*/

t_dlist	*ft_dlstnew(void *content, size_t content_size);
void	ft_dlstadd(t_dlist **dlst, t_dlist *add);
void	ft_rotate_mode(t_dlist **dlst);
void	ft_dlstiter(t_dlist *dlst, void (*f)(t_dlist *elem));
void	ft_rotate_mode_ln(t_dlist **dlst, int *ln);
void	ft_dlstiter_rot(t_dlist *dlst, void (*f)(t_dlist *elem));

/*
**		ft_tree
*/

#endif /* !LIBFT_H */
