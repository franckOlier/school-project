/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 13:04:22 by folier            #+#    #+#             */
/*   Updated: 2013/12/28 01:15:28 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

int		ft_strcmp(const char *str1, const char *str2)
{
	if (!str1)
		return (-1);
	if (!str2)
		return (1);
	if (!str1 && !str2)
		return (0);
	while (*str1 == *str2 && *str1 != '\0')
	{
		str1++;
		str2++;
	}
	if ((*str1 - *str2) != 0)
	{
		if ((*str1 - *str2) > 0)
			return (1);
		return (-1);
	}
	return (0);
}
