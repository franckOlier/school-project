/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 12:55:22 by folier            #+#    #+#             */
/*   Updated: 2013/12/18 17:51:46 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

size_t	ft_strlen(const char *str)
{
	size_t	nb;

	if (!str)
		return (0);
	nb = 0;
	while (str[nb])
		nb++;
	return (nb);
}
