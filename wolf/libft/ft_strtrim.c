/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:25:27 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 12:55:36 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"


static int	ft_check(const char s)
{
	if (s == ' ' || s == '\n' || s == '\t')
		return (1);
	return (0);
}

static int	ft_end(const char *s)
{
	while (*s)
	{
		if (ft_check(*s) == 1)
			s++;
		else
			return (0);
	}
	return (1);
}

char				*ft_strtrim(const char *s)
{
	char			*str;
	unsigned int	cnt;
	int				lock;

	cnt = 0;
	lock = 1;
	if (!s)
		return (NULL);
	str = ft_strnew((ft_strlen(s) + 1));
	while (*s)
	{
		while (ft_check(*s) == 1 && lock == 1)
			s++;
		lock = 0;
		if (ft_check(*s) == 1 && ft_end(s) == 1)
		{
			str[cnt] = '\0';
			return (str);
		}
		str[cnt++] = *s;
		s++;
	}
	str[cnt] = '\0';
	return (str);
}
