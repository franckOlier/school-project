/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstadd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/01 19:49:48 by folier            #+#    #+#             */
/*   Updated: 2014/01/01 20:14:59 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

void	ft_dlstadd(t_dlist **dlst, t_dlist *add)
{
	if (*dlst == NULL)
	{
		*dlst = add;
		return ;
	}
	if ((*dlst)->next != NULL)
	{
		ft_dlstadd(&(*dlst)->next, add);
		return ;
	}
	(*dlst)->next = add;
	add->prev = *dlst;
}
