/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str2dup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/27 21:23:49 by folier            #+#    #+#             */
/*   Updated: 2014/01/16 21:23:53 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"

static size_t	count_str2(char **str)
{
	size_t		nb;

	nb = 0;
	while (str[nb] != NULL)
		nb++;
	return (nb + 1);
}

char		**ft_str2dup(char **str)
{
	char	**new;
	int		ct;

	if (!str)
		return (NULL);
	ct = 0;
	new = (char **)malloc(count_str2(str) * sizeof(char *));
	if (!new)
		return (NULL);
	while (str[ct] != NULL)
	{
		new[ct] = ft_strdup(str[ct]);
		ct++;
	}
	new[ct] = NULL;
	return (new);
}
