/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 14:58:03 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 12:54:02 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

static size_t	ft_strnlen(const char *dst, size_t len)
{
	size_t		res;

	res = (ft_strlen(dst) <= len ? ft_strlen(dst) : len);
	return (res);
}

size_t		ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t	dst_ln;
	size_t	room;
	size_t	cp_ln;

	if (!dest || !src)
		return (0);
	dst_ln = ft_strnlen(dest, size);
	room = size - dst_ln;
	if (room == 0)
		return (dst_ln + ft_strlen(src));
	cp_ln = ft_strnlen(src, room - 1);
	ft_memcpy(dest + dst_ln, src, cp_ln);
	*(dest + dst_ln + cp_ln) = '\0';
	return (dst_ln + ft_strlen(src));
}
