/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 14:59:33 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:16:34 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "include/libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int			cnt;
	const char	*str;

	str = NULL;
	cnt = 0;
	while (s[cnt])
	{
		if (s[cnt] == (char)c)
			str = s + cnt;
		cnt++;
	}
	if ((char)c == 0 && s)
		return ((char *)s + cnt);
	return ((char *)str);
}
