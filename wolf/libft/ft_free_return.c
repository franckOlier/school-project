/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_return.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/29 15:32:08 by folier            #+#    #+#             */
/*   Updated: 2014/01/16 21:21:18 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"
#include "stdlib.h"

int			ft_free_return(char *str, int value)
{
	if (str)
		free(str);
	return (value);
}
