/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_fonction.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/16 07:50:55 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 20:45:08 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#define MAPY e->init->map_y
#define POS e->init->pos
#define DR e->init->dir
#define PLANE e->init->plane
#define TIME e->init->time

int		esc_fonc(t_env *e)
{
	(void)e;
	exit(0);
}

int		before_fonc(t_env *e)
{
	if ((MAPY[(int)POS[Y]].map_x[(int)(POS[X] + DR[X] * TIME->mvspeed)]) == 0)
		POS[X] = POS[X] + (DR[X] * TIME->mvspeed);
	if ((MAPY[(int)(POS[Y] + DR[Y] * TIME->mvspeed)].map_x[(int)POS[X]]) == 0)
		POS[Y] = POS[Y] + (DR[Y] * TIME->mvspeed);
	return (0);
}

int		back_fonc(t_env *e)
{
	if ((MAPY[(int)POS[Y]].map_x[(int)(POS[X] - DR[X] * TIME->mvspeed)]) == 0)
		POS[X] = POS[X] - (DR[X] * TIME->mvspeed);
	if ((MAPY[(int)(POS[Y] - DR[Y] * TIME->mvspeed)].map_x[(int)POS[X]]) == 0)
		POS[Y] = POS[Y] - (DR[Y] * TIME->mvspeed);
	return (0);
}

int		left_fonc(t_env *e)
{
	double	tmpdir_x;
	double	tmplane_x;

	tmpdir_x = DR[X];
	tmplane_x = PLANE[X];
	DR[X] = (DR[X] * cos(-TIME->rotspeed)) - (DR[Y] * sin(-TIME->rotspeed));
	DR[Y] = tmpdir_x * sin(-TIME->rotspeed) + (DR[Y] * cos(-TIME->rotspeed));
	PLANE[X] = (PLANE[X] * cos(-TIME->rotspeed))
		- PLANE[Y] * sin(-TIME->rotspeed);
	PLANE[Y] = (tmplane_x * sin(-TIME->rotspeed))
		+ (PLANE[Y] * cos(-TIME->rotspeed));
	return (0);
}

int		right_fonc(t_env *e)
{
	double	tmpdir_x;
	double	tmplane_x;

	tmpdir_x = DR[X];
	tmplane_x = PLANE[X];
	DR[X] = (DR[X] * cos(TIME->rotspeed)) - (DR[Y] * sin(TIME->rotspeed));
	DR[Y] = tmpdir_x * sin(TIME->rotspeed) + (DR[Y] * cos(TIME->rotspeed));
	PLANE[X] = (PLANE[X] * cos(TIME->rotspeed))
		- PLANE[Y] * sin(TIME->rotspeed);
	PLANE[Y] = (tmplane_x * sin(TIME->rotspeed))
		+ (PLANE[Y] * cos(TIME->rotspeed));
	return (0);
}
