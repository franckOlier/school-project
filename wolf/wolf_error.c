/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf_error.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 07:46:02 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 20:46:20 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static const t_error		*error_table(void)
{
	static const t_error	error[] =		{
	{29, "wolf3d: malloc fail in fonc: "	}
	};
	return (error);
}

int				wolf_error(int error, char *txt_error)
{
	t_error		*data;

	data = error_table();
	write(2, data[error].error, data[error].size);
	if (txt_error)
		ft_putstr_fd(txt_error, 2);
	write(2, "\n", 1);
	return (0);
}
