/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/15 07:17:37 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 20:47:31 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#define BPP &(e->pix->bpp)
#define SZLN &(e->pix->szln)
#define END &(e->pix->end)

static t_wall	*init_wall(void)
{
	t_wall		*new;

	if ((new = malloc(sizeof(t_wall))) == NULL)
	{
		(void)wolf_error(0, "init_wall");
		exit(0);
	}
	return (new);
}

static t_img	*init_pix(void)
{
	t_img		*new;

	if ((new = malloc(sizeof(t_img))) == NULL)
	{
		(void)wolf_error(0, "init_pix");
		exit(0);
	}
	return (new);
}

static t_time	*init_time(void)
{
	t_time		*time;

	if ((time = malloc(sizeof(t_time))) == NULL)
	{
		(void)wolf_error(0, "init_time");
		exit(0);
	}
	time->ftime = 0.06;
	time->mvspeed = time->ftime * 5.0;
	time->rotspeed = time->ftime * 1.0;
	return (time);
}

static t_data	*init_env(void)
{
	t_data		*init;

	if ((init = malloc(sizeof(t_data))) == NULL)
	{
		(void)wolf_error(0, "init_env");
		exit(0);
	}
	init->map_y = world_one();
	init->pos[X] = 2;
	init->pos[Y] = 2;
	init->dir[X] = 1;
	init->dir[Y] = 0;
	init->plane[X] = 0;
	init->plane[Y] = 0.66;
	init->time = init_time();
	return (init);
}

void			init_win(t_env *e)
{
	if (!e)
	{
		if ((e = (t_env *)malloc(sizeof(t_env))) == NULL)
			return ((void)wolf_error(0, "init_win"));
	}
	if ((e->ray = malloc(sizeof(t_calc))) == NULL)
		return ((void)wolf_error(0, "init_ray"));
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, WIN_WIDTH, WIN_HEIGHT, WIN_NAME);
	e->img = mlx_new_image(e->mlx, WIN_WIDTH, WIN_HEIGHT);
	e->init = init_env();
	e->pix = init_pix();
	e->wall = init_wall();
	mlx_expose_hook(e->win, expose_hook, e);
	mlx_hook(e->win, 2, 3, key_hook, e);
	e->pix->img = mlx_get_data_addr(e->img, BPP, SZLN, END);
	mlx_loop_hook(e->mlx, heart_wolf, e);
	mlx_loop(e->mlx);
}
