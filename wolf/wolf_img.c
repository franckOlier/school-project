	/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf_img.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/18 11:51:30 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 20:47:19 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#define U_CHAR unsigned char
#define STATUS e->ray->status

static void	set_color(t_img *pix, U_CHAR red, U_CHAR green, U_CHAR blue)
{
	pix->red = red;
	pix->green = green;
	pix->blue = blue;
}

static void	put_pix_img(t_img *pix, int y, int x)
{
	pix->img[y * pix->szln + x * (pix->bpp / 8) + 2] = pix->red;
	pix->img[y * pix->szln + x * (pix->bpp / 8) + 1] = pix->green;
	pix->img[y * pix->szln + x * (pix->bpp / 8) + 0] = pix->blue;
}

static void	wall_color(t_img *pix, t_env *e, int x)
{
	if (STATUS[SIDE] == 1)
	{
		if (e->ray->ray_dir[Y] > 0)
			set_color(pix, 150, 0, 24);
		else
			set_color(pix, 255, 203, 96);
	}
	else
	{
		if (e->ray->ray_dir[X] > 0)
			set_color(pix, 84, 114, 174);
		else
			set_color(pix, 114, 62, 100);
	}
}

void		draw_column_img(t_wall *wall, t_img *pix, int x, t_env *e)
{
	int		y;

	y = 0;
	while (y < WIN_HEIGHT)
	{
		if (y < wall->draw_start)
		{
			set_color(pix, 204, 204, 255);
			put_pix_img(pix, y, x);
		}
		else if (y > wall->draw_end)
		{
			set_color(pix, 255, 239, 213);
			put_pix_img(pix, y, x);
		}
		else
		{
			wall_color(pix, e, x);
			put_pix_img(pix, y, x);
		}
		y++;
	}
}
