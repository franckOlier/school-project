/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_fonc1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/17 15:54:38 by folier            #+#    #+#             */
/*   Updated: 2014/02/23 21:11:00 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			lem_print(char *str)
{
	int		i;

	i = 0;
	while (str[i] && ft_isprint((int)str[i]) && str[i] != '-')
		i++;
	if (str[i] == '\0')
		return (1);
	return (0);
}

int			lem_digit(char *str)
{
	int		i;

	i = 0;
	if (str[i] == '-')
		i++;
	while (str[i] && ft_isdigit((int)str[i]))
		i++;
	if (str[i] == '\0')
		return (1);
	return (0);
}

int			match_node(t_lem **lem, char *ori, char *dst)
{
	t_node	*tmp;

	tmp = (*lem)->room;
	if (tmp == NULL)
		return (1);
	while (tmp)
	{
		if (ft_strcmp(tmp->name, ori) == 0)
		{
			tmp->path = add_path(tmp, dst);
			return (1);
		}
		tmp = tmp->next;
	}
	return (0);
}

void		init_node(t_node **node)
{
	t_node	*new;

	if ((new = (t_node *)malloc(sizeof(t_node))) == NULL)
		return ;
	if ((new->path = (char **)malloc(sizeof(char *))) == NULL)
	{
		free(new);
		return ;
	}
	new->next = NULL;
	new->name = NULL;
	new->path[0] = NULL;
	new->check = FALSE;
	new->use = FALSE;
	*node = new;
}
