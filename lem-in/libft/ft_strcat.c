/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 14:53:10 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:14:26 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

char	*ft_strcat(char *s1, const char *s2)
{
	if (!s1 || !s2)
		return (NULL);
	ft_strncpy(s1 + ft_strlen(s1), s2, (ft_strlen(s2) + 1));
	return (s1);
}
