/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 13:35:59 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:13:29 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	void	*tmp;

	tmp = malloc(n);
	if (tmp == NULL)
		return (NULL);
	ft_memcpy(s1, ft_memcpy(tmp, s2, n), n);
	free(tmp);
	return (s1);
}
