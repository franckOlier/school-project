/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 18:51:50 by folier            #+#    #+#             */
/*   Updated: 2014/02/12 16:58:28 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

static int	test_str(const char str)
{
	if (str == '\n' || str == '\v' || str == '\t' || str == '\r' ||
			str == '\f' || str == ' ' )
		return (1);
	return (0);
}

int			ft_atoi(const char *str)
{
	int		nb;
	int		count;
	int		sg;

	sg = 1;
	count = 0;
	nb = 0;
	while (test_str(str[count]))
		count++;
	if (str[count] == '-' || str[count] == '+')
	{
		sg = (str[count] == '-' ? -1 : 1);
		count++;
	}
	while (ft_isdigit(str[count]))
	{
		nb = (nb * 10) + (str[count] - '0');
		count++;
	}
	return (nb*sg);
}
