/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 13:33:03 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:16:00 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

int		ft_strncmp(const char *str1, const char *str2, size_t size)
{
	while (*str1 == *str2 && *str1 != '\0' && size > 0)
	{
		str1++;
		str2++;
		size--;
	}
	if ((*str1 - *str2) != 0 && size > 0)
	{
		if (*str1 - *str2 > 0)
			return (1);
		return (-1);
	}
	return (0);
}
