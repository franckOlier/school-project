/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 19:03:20 by folier            #+#    #+#             */
/*   Updated: 2014/01/19 12:51:26 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*list;

	if (!alst || !*alst)
		return ;
	while (*alst)
	{
		list = *alst;
		del(list->content, list->content_size);
		*alst = list->next;
		free(list);
		list = NULL;
	}
	*alst = NULL;
}
