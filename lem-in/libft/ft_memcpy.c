/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 11:07:10 by folier            #+#    #+#             */
/*   Updated: 2014/03/05 15:35:19 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

void	*ft_memcpy(void *s1, const void *s2, size_t n)
{
	unsigned char		*str1;
	const unsigned char	*str2;

	if (!s1 || !s2)
		return (NULL);
	str1 = (unsigned char *)s1;
	str2 = (const unsigned char *)s2;
	while (n > 0)
	{
		*str1 = *str2;
		str1++;
		str2++;
		n--;
	}
	return (s1);
}
