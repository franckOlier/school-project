/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate_mode_ln.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/05 12:46:10 by folier            #+#    #+#             */
/*   Updated: 2014/01/05 13:02:50 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

void	ft_rotate_mode_ln(t_dlist **dlist, int *ln)
{
	t_dlist		*tmp;
	int			count;

	*ln = 0;
	if (!*dlist)
		return ;
	tmp = *dlist;
	count = 1;
	while (tmp->next != NULL)
	{
		tmp = tmp->next;
		count++;
	}
	*ln = count;
	(*dlist)->prev = tmp;
	tmp->next = *dlist;
}
