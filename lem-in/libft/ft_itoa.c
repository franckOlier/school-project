/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 13:57:33 by folier            #+#    #+#             */
/*   Updated: 2014/02/23 23:16:12 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"

static unsigned int		ft_size_m(int n)
{
	unsigned int		aux;
	unsigned int		nb;

	aux = n > 0 ? 0 : 1;
	nb = n;
	if (n < 0 && n != -2147483648)
		nb = -n;
	while (nb > 10)
	{
		nb = nb / 10;
		aux++;
	}
	return (aux);
}

char					*ft_itoa(int n)
{
	char				*str;
	unsigned int		aux;

	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	if (n == 0)
		return (ft_strdup("0"));
	aux = ft_size_m(n);
	str = (char *)malloc(sizeof(char) * (aux + 2));
	if (!str)
		return (NULL);
	if (n < 0 && n != -2147483648)
	{
		str[0] = '-';
		n = -n;
	}
	str[aux + 1] = '\0';
	while (n > 10)
	{
		str[aux--] = ((n % 10) + '0');
		n = n / 10;
	}
	str[aux] = n + '0';
	return (str);
}
