/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 14:54:50 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:15:55 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

char	*ft_strncat(char *dst, const char *src, size_t size)
{
	size_t	cnt2;
	size_t	cnt;

	if (!dst)
		return (NULL);
	cnt = 0;
	cnt2 = ft_strlen(dst);
	while (src[cnt] && cnt < size)
	{
		dst[cnt2] = src[cnt];
		cnt2++;
		cnt++;
	}
	dst[cnt2] = '\0';
	return (dst);
}
