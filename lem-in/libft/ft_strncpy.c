/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 17:51:32 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:16:05 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

char	*ft_strncpy(char *s1, const char *s2, size_t n)
{
	size_t	pos;

	pos = 0;
	while (s2[pos] && pos < n)
	{
		s1[pos] = s2[pos];
		pos++;
	}
	while (pos < n)
		s1[pos++] = '\0';
	return (s1);
}
