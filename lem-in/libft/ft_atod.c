/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atod.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/12 16:16:05 by folier            #+#    #+#             */
/*   Updated: 2014/02/23 23:14:47 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

static int		atod_pow(int base, int power)
{
	int			res;

	if (power == 0)
		res = 1;
	else
		res = base * atod_pow(base, power -1);
	return (res);
}

static double	convert_dec(double dec)
{
	double		tmp;
	int			power;

	tmp = dec;
	power = 1;
	while (tmp > 10)
	{
		tmp = tmp / 10;
		power++;
	}
	dec = dec / atod_pow(10, power);
	return (dec);
}

double			ft_atod(const char *str)
{
	double		ent;
	double		dec;
	char		**tmp;
	int			err;

	if ((tmp = ft_strsplit_ac(str, '.', &err)) == NULL)
		return (0);
	ent = (double)ft_atoi(tmp[0]);
	if (err == 2)
	{
		dec = (double)ft_atoi(tmp[1]);
		dec = convert_dec(dec);
	}
	else
		dec = 0;
	dec = ent + dec;
	return (dec);
}
