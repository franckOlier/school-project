/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 17:51:32 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:14:45 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

char	*ft_strcpy(char *s1, const char *s2)
{
	size_t	pos;

	pos = 0;
	if (!s1 || !s2)
		return (NULL);
	while (s2[pos])
	{
		s1[pos] = s2[pos];
		pos++;
	}
	s1[pos] = '\0';
	return (s1);
}
