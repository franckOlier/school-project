/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_strjoin.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/29 14:04:45 by folier            #+#    #+#             */
/*   Updated: 2014/01/30 08:07:04 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"
#include "stdlib.h"

char	*ft_free_strjoin(char *s1, char *s2, const int op)
{
	char	*tmp;
	char	*tmp2;
	char	*new;

	tmp = s1;
	tmp2 = s2;
	new = ft_strjoin(s1, s2);
	if (op == 1)
		free(tmp);
	if (op == 2)
		free(tmp2);
	if (op == 3)
	{
		free(tmp);
		free(tmp2);
	}
	return (new);
}
