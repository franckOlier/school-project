/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 16:23:15 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:17:30 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "include/libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	int		c1;
	int		c2;

	c1 = 0;
	c2 = 0;
	while (s2[c2])
	{
		if (!s1[c1])
			return (NULL);
		if (s1[c1] == s2[c2])
			c2++;
		else
		{
			c1 = c1 - c2;
			c2 = 0;
		}
		c1++;
	}
	return ((char *)(s1 + c1 - c2));
}
