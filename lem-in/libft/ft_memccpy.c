/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 13:08:28 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:12:56 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "include/libft.h"

void	*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	unsigned char		*str1;
	const unsigned char	*str2;

	str1 = (unsigned char *)s1;
	str2 = (const unsigned char *)s2;
	if (!s1 || !s2)
		return (NULL);
	while (n > 0)
	{
		*str1 = *str2;
		if (*str2 == c)
			return (str1 + 1);
		str1++;
		str2++;
		n--;
	}
	return (NULL);
}
