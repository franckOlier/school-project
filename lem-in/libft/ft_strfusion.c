/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strfusion.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/27 21:23:49 by folier            #+#    #+#             */
/*   Updated: 2014/02/23 23:20:24 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"
#define SZ_C sizeof(char *)

static size_t	cnt_st(char **str)
{
	size_t		nb;

	nb = 0;
	while (str[nb] != NULL)
		nb++;
	return (nb + 3);
}

void			ft_strfusion(char **str, char *add)
{
	int			ct;

	if (!str)
		return ;
	ct = 0;
	if ((str = (char **)realloc(str , (cnt_st(str) * SZ_C))) == NULL)
		return ;
	while (str[ct] != NULL)
		ct++;
	str[ct++] = ft_strdup(add);
	str[ct] = NULL;
	return ;
}
