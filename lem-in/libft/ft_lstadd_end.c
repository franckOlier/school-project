/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 18:55:26 by folier            #+#    #+#             */
/*   Updated: 2014/03/05 15:38:25 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

void		ft_lstadd_end(t_list **list, t_list *new)
{
	t_list	*tmp;

	if (*list == NULL)
	{
		*list = new;
		return ;
	}
	if (!new)
		return ;
	tmp = *list;
	while ((*list)->next)
		*list = (*list)->next;
	(*list)->next = new;
	*list = tmp;
}
