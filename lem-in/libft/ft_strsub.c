/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:20:04 by folier            #+#    #+#             */
/*   Updated: 2014/02/11 17:50:05 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>
#include <stdlib.h>
#include "include/libft.h"

char				*ft_strsub(const char *s1, unsigned int start, size_t len)
{
	char			*str;
	unsigned int	cnt;

	if (!s1)
		return (NULL);
	cnt = start + len;
	if ((str = (char *)malloc(sizeof(char) * (len + 1))) == NULL)
		return (NULL);
	while (cnt > start)
	{
		*str = s1[start];
		start++;
		str++;
	}
	*str = '\0';
	return (str - len);
}
