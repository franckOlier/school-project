/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:22:18 by folier            #+#    #+#             */
/*   Updated: 2014/01/16 21:25:00 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "include/libft.h"
#define STLN(X) ft_strlen(X)

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*str;
	size_t	cnt;

	cnt = 0;
	if (!s1 || !s2)
		return (NULL);
	str = (char *)malloc((STLN(s1) + STLN(s2) + 2) * sizeof(char));
	if (!str)
		return (NULL);
	while (*s1)
	{
		str[cnt] = *s1;
		cnt++;
		s1++;
	}
	while (*s2)
	{
		str[cnt] = *s2;
		cnt++;
		s2++;
	}
	str[cnt] = '\0';
	return (str);
}
