/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlstiter.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/03 23:05:28 by folier            #+#    #+#             */
/*   Updated: 2014/01/16 21:24:40 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

void		ft_dlstiter(t_dlist *dlst, void (*f)(t_dlist *elem))
{
	f(dlst);
	if (dlst->next != NULL)
		ft_dlstiter(dlst->next, f);
}
