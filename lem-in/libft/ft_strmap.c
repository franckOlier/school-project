/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:13:53 by folier            #+#    #+#             */
/*   Updated: 2013/12/12 15:15:44 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*dest;
	int		cnt;

	if (!s || !f)
		return (NULL);
	cnt = 0;
	dest = ft_strnew(ft_strlen(s));
	while (s[cnt])
	{
		dest[cnt] = f(s[cnt]);
		cnt++;
	}
	return (dest);
}
