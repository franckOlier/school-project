/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcut.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/29 09:33:47 by folier            #+#    #+#             */
/*   Updated: 2013/12/29 09:59:07 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

char		*ft_strcut(const char *str, size_t cut)
{
	char	*new;

	if (!str)
		return (NULL);
	if (ft_strlen(str) < cut)
		return (ft_strdup(str));
	new = ft_strdup(str);
	while (str[cut])
	{
		new[cut] = '\0';
		cut++;
	}
	return (new);
}
