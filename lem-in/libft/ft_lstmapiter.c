/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmapiter.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 23:07:39 by folier            #+#    #+#             */
/*   Updated: 2014/03/05 15:38:38 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "include/libft.h"

void		ft_lstmapiter(t_list *list, void (*f)(t_list *))
{
	t_list	*tmp;
	
	if (!list || !f)
		return ;
	tmp = list;
	while (tmp)
	{
		f(tmp);
		tmp = tmp->next;
	}
}
