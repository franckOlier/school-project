/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 21:28:00 by folier            #+#    #+#             */
/*   Updated: 2014/03/23 18:18:41 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include "libft/include/libft.h"
# include <unistd.h>
# include <stdlib.h>
# define X coord[0]
# define Y coord[1]
# define FALSE 0
# define TRUE 1

typedef struct s_node	t_node;
typedef struct s_ltree	t_ltree;
typedef struct s_exil	t_exil;

typedef struct		s_err
{
	int				size;
	char			*str;
}					t_err;

struct				s_node
{
	char			*name;
	int				coord[2];
	char			**path;
	int				check;
	int				use;
	t_node			*next;
};

typedef struct		s_lem
{
	int				lem;
	char			*err;
	t_list			*map;
	t_node			*room;
	t_node			*start;
	t_node			*end;
	int				max[2];
}					t_lem;

struct				s_exil
{
	int				lem;
	int				ind;
};

struct				s_ltree
{
	t_node			*room;
	t_ltree			**sun;
};

/*
**		LEM_ERROR
*/

int		error_ret(char *str, int col);
void	print_path(t_ltree ***tree, t_lem *lem);
int		error_ret_spe(char *str, int col, int op);

/*
**		LEM_PARS
*/

typedef int (*t_fonc)(t_lem **, char **, int *);
int		lem_pars(t_lem **lem, char **line, int *op);
int		lem_init(t_lem **lem, char **line, int *op);
int		lem_cmd(t_lem **lem, char **line, int *op);
int		lem_comment(t_lem **lem, char **line, int *op);
int		lem_room(t_lem **lem, char **line, int *op);
int		lem_path(t_lem **lem, char **line, int *op);
void	node_add(t_lem **lem, t_node *add);

/*
**		LEM_PARS_FONC
*/

int		lem_digit(char *str);
int		lem_print(char *str);
void	init_node(t_node **node);
int		match_node(t_lem **lem, char *str1, char *str2);
int		chk_nm(char *name, t_lem *lem, int *op);
int		check_valide_lem(t_lem *lem);
int		lem_tablen(char **room);
char	**add_path(t_node *room, char *add);
void	new_line(t_lem **lem, char **line);
void	putlist(t_list *list);

/*
**		LEM_FONC
*/

void	putlist(t_list *list);
void	rec_data(t_lem *lem);

/*
**		LEM_LTREE && LEM_LIST
*/

t_ltree	*create_ltree(t_node *room);
void	insert_ltree(t_ltree **lem_in, t_ltree *add);
void	add_room_adj(t_lem *lem, t_list **queue, t_ltree *currenyyt);
void	lem_add_list(t_list **queue, t_ltree *add);
void	queue_free(t_list *queue);
void	maj_statut_room(t_lem *lem, t_ltree **path);

/*
**		LEM_IN
*/

void	set_path(t_lem *lem, t_ltree ***path);
t_ltree	**find_path(t_ltree *tree, t_node *end);
int		check_path_of_exil(t_ltree ***path);
void	exil_of_lem(t_ltree **path, t_lem *lem);
void	print_lem(int lem, char *room, int end);
void	move_lem(t_exil *tab, t_ltree **path, t_lem *lem);

#endif /* !LEM_IN_H */
