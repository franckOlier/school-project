/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/22 17:09:53 by folier            #+#    #+#             */
/*   Updated: 2014/03/22 09:38:06 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#define MAX lem->max[0]
#define PATH current->room->path

static t_ltree	*depop_list(t_list **queue)
{
	t_ltree		*back;
	t_list		*tmp;

	tmp = *queue;
	if (!*queue)
		return (NULL);
	back = (t_ltree *)(*queue)->content;
	*queue = (*queue)->next;
	free(tmp);
	return (back);
}

void			set_path(t_lem *lem, t_ltree ***path)
{
	t_ltree		*tree;
	t_ltree		*node;
	t_list		*queue;

	if (!MAX)
		return ;
	queue = NULL;
	tree = NULL;
	node = create_ltree(lem->start);
	tree = node;
	lem_add_list(&queue, node);
	while (queue)
	{
		node = depop_list(&queue);
		node->room->check = TRUE;
		if (node->room == lem->end)
		{
			queue_free(queue);
			path[MAX - 1] = find_path(tree, lem->end);
			maj_statut_room(lem, path[MAX - 1]);
			MAX = MAX - 1;
			return (set_path(lem, path));
		}
		add_room_adj(lem, &queue, node);
	}
}

static void		end_match(int *size, t_ltree ***path, t_ltree *tree)
{
	t_ltree		**new;

	if (!(new = (t_ltree **)malloc(sizeof(t_ltree *) * (*size + 1))))
		return ;
	new[*size] = NULL;
	new[*size - 1] = tree;
	*size = 0;
	*path = new;
}

t_ltree			**find_path(t_ltree *tree, t_node *end)
{
	static int	size_path = 0;
	int			size_loc;
	int			i;
	t_ltree		**path;

	++size_path;
	size_loc = size_path;
	if (tree->room == end)
	{
		end_match(&size_path, &path, tree);
		return (path);
	}
	i = 0;
	while (tree->sun[i])
	{
		if ((path = find_path(tree->sun[i], end)))
		{
			path[size_loc - 1] = tree;
			return (path);
		}
		size_path--;
		i++;
	}
	return (NULL);
}

void			add_room_adj(t_lem *lem, t_list **queue, t_ltree *current)
{
	t_ltree		*new;
	int			ct;
	t_node		*tmp;

	ct = 0;
	while (PATH[ct])
	{
		tmp = lem->room;
		while (tmp)
		{
			if (ft_strcmp(tmp->name, PATH[ct]) == 0 && tmp->check == FALSE)
			{
				new = create_ltree(tmp);
				insert_ltree(&current, new);
				lem_add_list(queue, new);
			}
			tmp = tmp->next;
		}
		ct++;
	}
}
