/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_error.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/17 13:15:23 by folier            #+#    #+#             */
/*   Updated: 2014/03/23 19:21:27 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_err		*err_tab(void)
{
	static t_err	err[] = {
		{61, "lem-in: syntax ./lem-in < map_ant.map | ./visu_hex (optionel)"},
		{53, "lem-in: error map_ant.map invalid instruction line: "			},
		{35, "lem-in: error map_ant.map needed: "							},
		{26, "lem-in: error invalid path"									},
		{0, ""																},
		{33, "lem-in: error invalid room name: "							}
	};
	return (err);
}

int					error_ret(char *str, int col)
{
	t_err			*err;

	err = err_tab();
	write(2, err[col].str, err[col].size);
	if (str)
		ft_putstr_fd(str, 2);
	write(2, "\n", 1);
	return (1);
}

int					error_ret_spe(char *str, int col, int op)
{
	t_err			*err;

	if (op == 7)
		col = 5;
	err = err_tab();
	write(2, err[col].str, err[col].size);
	if (str)
		ft_putstr_fd(str, 2);
	write(2, "\n", 1);
	return (1);
}
