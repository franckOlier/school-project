/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_ltree1.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/23 15:35:10 by folier            #+#    #+#             */
/*   Updated: 2014/03/04 20:10:15 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_ltree		*create_ltree(t_node *room)
{
	t_ltree	*new;

	if ((new = (t_ltree *)malloc(sizeof(t_ltree))) == NULL)
		return (NULL);
	if ((new->sun = (t_ltree **)malloc(sizeof(t_ltree *))) == NULL)
	{
		free(new);
		return (NULL);
	}
	new->sun[0] = NULL;
	new->room = room;
	return (new);
}

static int	ltree_ln(t_ltree **count)
{
	int		size;

	size = 0;
	while (count[size])
		size++;
	return (size);
}

void		insert_ltree(t_ltree **lem_in, t_ltree *new)
{
	t_ltree	**old;
	int		size;

	old = (*lem_in)->sun;
	size = ltree_ln(old) + 2;
	if (!((*lem_in)->sun = (t_ltree **)malloc(sizeof(t_ltree *) * (size))))
		return ;
	(*lem_in)->sun[--size] = NULL;
	(*lem_in)->sun[--size] = new;
	while (old[0] != (old[size]))
	{
		size--;
		(*lem_in)->sun[size] = old[size];
	}
}

void		lem_add_list(t_list **queue, t_ltree *add)
{
	t_list	*new;
	t_list	*tmp;

	if ((new = (t_list *)malloc(sizeof(t_list))) == NULL)
		return ;
	new->content = add;
	new->next = NULL;
	new->content_size = sizeof(t_ltree);
	if (*queue == NULL)
	{
		*queue = new;
		return ;
	}
	tmp = *queue;
	while (tmp->next != NULL)
		tmp = tmp->next;
	tmp->next = new;
}
