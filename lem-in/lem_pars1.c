/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_pars1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/17 14:52:28 by folier            #+#    #+#             */
/*   Updated: 2014/03/22 09:58:26 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int					lem_pars(t_lem **lem, char **line, int *op)
{
	size_t			i;
	static t_fonc	fonc_tab[] =
	{
		lem_comment,
		lem_init,
		lem_cmd,
		lem_room,
		lem_path
	};

	i = 0;
	while (i < (sizeof(fonc_tab) / sizeof(t_fonc)))
	{
		if (fonc_tab[i](lem, line, op))
		{
			return (0);
		}
		i++;
	}
	if (*op == 0 || *op == 6)
		return (0);
	return (1);
}

void				node_add(t_lem **lem, t_node *add)
{
	t_node			*tmp;

	tmp = (*lem)->room;
	if ((*lem)->room == NULL)
	{
		(*lem)->room = add;
		return ;
	}
	while (tmp->next != NULL)
		tmp = tmp->next;
	tmp->next = add;
}

void	putlist(t_list *list)
{
	ft_putendl((char *)list->content);
}

void	new_line(t_lem **lem, char **line)
{
	t_list		*new;

	new = ft_lstnew(*line, (ft_strlen(*line) + 1));
	ft_lstadd_end(&((*lem)->map), new);
}
