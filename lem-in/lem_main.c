/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_main.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 21:30:53 by folier            #+#    #+#             */
/*   Updated: 2015/09/18 11:02:30 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int		build_struct(t_lem **lem)
{
	t_lem		*new;
	char		*line;
	int			op;

	op = 5;
	if ((new = (t_lem *)malloc(sizeof(t_lem))) == NULL)
		return (1);
	new->room = NULL;
	new->start = NULL;
	new->end = NULL;
	new->err = ft_strdup("ERROR");
	new->map = NULL;
	new->lem = 0;
	while (ft_get_next_line(0, &line) > 0 && op != 6)
	{
		if (lem_pars(&new, &line, &op))
			return (error_ret_spe(new->err, 4, op));
		if (line)
			free(line);
	}
	rec_data(new);
	*lem = new;
	return (0);
}

static t_ltree	***init_lem_int(t_lem *lem)
{
	t_ltree		***new;
	int			i;

	i = 0;
	if (!(new = (t_ltree ***)malloc(sizeof(t_ltree **) * (lem->max[1] + 1))))
		return (NULL);
	while (i <= lem->max[1])
		new[i++] = NULL;
	return (new);
}

int				main(int ac, char **av)
{
	t_lem		*lem;
	t_ltree		***lem_in;

	(void)av;
	if (ac != 1)
		return (error_ret(NULL, 0));
	if (build_struct(&lem))
		return (1);
	if (check_valide_lem(lem))
		return (error_ret(lem->err, 2));
	lem_in = init_lem_int(lem);
	ft_lstmapiter(lem->map, putlist);
	set_path(lem, lem_in);
	if (check_path_of_exil(lem_in))
		return (error_ret(NULL, 3));
	ft_lstmapiter(lem->map, putlist);
	write(1, "\n", 1);
	exil_of_lem(lem_in[0], lem);
	return (0);
}
