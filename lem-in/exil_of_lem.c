/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exil_of_lem.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/23 20:34:23 by folier            #+#    #+#             */
/*   Updated: 2014/03/16 12:06:46 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int				check_path_of_exil(t_ltree ***path)
{
	if (path[0])
		return (0);
	return (1);
}

void			print_lem(int lem, char *room, int end)
{
	write(1, "L", 1);
	ft_putnbr(lem);
	write(1, "-", 1);
	ft_putstr(room);
	if (end == 0)
		write(1, " ", 1);
}

static t_exil	*init_lemess(int lem)
{
	t_exil		*new;

	if (!(new = (t_exil *)malloc(sizeof(t_exil))))
		return (NULL);
	new->lem = lem;
	new->ind = 0;
	return (new);
}

static t_exil	**init_leminator(int lem)
{
	t_exil		**new;
	int			i;

	i = 1;
	if (!(new = (t_exil **)malloc(sizeof(t_exil) * (lem + 1))))
		return (NULL);
	new[lem] = NULL;
	while (i <= lem)
	{
		new[i - 1] = init_lemess(i);
		i++;
	}
	return (new);
}

void			exil_of_lem(t_ltree **path, t_lem *lem)
{
	t_exil		**lem_in;
	int			lemi;
	int			start;

	lem_in = init_leminator(lem->lem);
	while (lem_in[lem->lem - 1]->ind != -1)
	{
		start = 0;
		lemi = 0;
		while (lemi < lem->lem)
		{
			if ((start == 0) && (lem_in[lemi]->ind == 0))
			{
				start = 1;
				move_lem(lem_in[lemi], path, lem);
			}
			else if (lem_in[lemi]->ind > 0)
				move_lem(lem_in[lemi], path, lem);
			lemi++;
		}
		write (1, "\n", 1);
	}
}
