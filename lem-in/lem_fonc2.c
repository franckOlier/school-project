/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_fonc2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/22 19:34:15 by folier            #+#    #+#             */
/*   Updated: 2014/02/23 23:10:29 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int				lem_tablen(char **tab)
{
	int			len;

	len = 0;
	while (tab[len])
		len++;
	return (len);
}

static int		check_valide_path(char **path, char *name, char *add)
{
	int			ct;
	char		**tmp;

	ct = 0;
	tmp = path;
	if (ft_strcmp(name, add) == 0)
		return (1);
	while (tmp[ct])
	{
		if (ft_strcmp(tmp[ct], add) == 0)
			return (1);
		ct++;
	}
	return (0);
}

char			**add_path(t_node *room, char *add)
{
	char		**old;
	char		**new;
	int			size;

	old = room->path;
	if (check_valide_path(room->path, room->name, add))
		return (old);
	size = lem_tablen(old) + 2;
	if ((new = (char **)malloc(sizeof(char *) * (size))) == NULL)
		return (NULL);
	new[--size] = NULL;
	new[--size] = add;
	while (*old != old[size])
	{
		size--;
		new[size] = old[size];
	}
	return (new);
}
