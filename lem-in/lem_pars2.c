/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_pars2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/17 15:32:20 by folier            #+#    #+#             */
/*   Updated: 2014/03/23 19:36:28 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#define PRT lem_print(tp[0])
#define DGT lem_digit
#define MTCH match_node

int			lem_init(t_lem **lem, char **line, int *op)
{
	if (**line == '\n' || **line == '\r' || **line == '\0')
	{
		(*lem)->err = ft_strdup("ERROR");
		*op = 6;
		return (0);
	}
	if (lem_digit(*line) && *op == 5 && **line != '-')
	{
		(*lem)->lem = ft_atoi(*line);
		new_line(lem, line);
		*op = 1;
		return (1);
	}
	if (*op != 3 && *op != 2 && *op != 0)
		*op = 1;
	return (0);
}

int			lem_cmd(t_lem **lem, char **line, int *op)
{
	(void)lem;
	if (*op >= 5)
	{
		(*lem)->err = ft_strdup("ERROR");
		return (0);
	}
	if (line[0][0] == '#' && line[0][1] == '#' && lem_print(*line + 2))
	{
		if (ft_strncmp("start", *line + 2, 5) == 0 && *op != 0)
			*op = 2;
		if (ft_strncmp("end", *line + 2, 3) == 0 && *op != 0)
			*op = 3;
		new_line(lem, line);
		return (1);
	}
	return (0);
}

int			lem_comment(t_lem **lem, char **line, int *op)
{
	(void)lem;
	(void)op;
	if (line[0][0] == '#' && line[0][1] != '#')
	{
		new_line(lem, line);
		return (1);
	}
	return (0);
}

int			lem_room(t_lem **lem, char **line, int *op)
{
	char	**tp;
	int		ac;
	t_node	*new;

	if ((*op != 1 && *op != 2 && *op != 3) || *op >= 5)
		return (0);
	tp = ft_strsplit_ac(*line, ' ', &ac);
	if (ac == 3 && PRT && DGT(tp[1]) && DGT(tp[2]) && chk_nm(tp[0], *lem, op))
	{
		init_node(&new);
		new->name = ft_strdup(tp[0]);
		new->X = ft_atoi(tp[1]);
		new->Y = ft_atoi(tp[2]);
		node_add(lem, new);
		if (*op == 2 && (*lem)->start == NULL)
			(*lem)->start = new;
		if (*op == 3 && (*lem)->end == NULL)
			(*lem)->end = new;
		*op = 1;
		free(tp);
		new_line(lem, line);
		return (1);
	}
	free(tp);
	return (0);
}

int			lem_path(t_lem **lem, char **line, int *op)
{
	char	**tmp;
	int		ac;

	if (*op >= 5)
		return (0);
	*op = 0;
	tmp = ft_strsplit_ac(*line, '-', &ac);
	if (ac == 2 && MTCH(lem, tmp[0], tmp[1]) && MTCH(lem, tmp[1], tmp[0]))
	{
		new_line(lem, line);
		return (1);
	}
	else
	{
		*op = 6;
		new_line(lem, line);
	}
	free(tmp);
	return (0);
}
