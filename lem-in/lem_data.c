/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_data.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/22 15:47:41 by folier            #+#    #+#             */
/*   Updated: 2014/03/15 16:55:27 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		rec_data(t_lem *lem)
{
	int		max[2];

	if (lem->end == NULL || lem->start == NULL)
		return ;
	max[0] = lem_tablen(lem->end->path);
	max[1] = lem_tablen(lem->start->path);
	if (max[0] > max[1])
	{
		lem->max[0] = 1;
		lem->max[1] = 1;
	}
	else
	{
		lem->max[0] = 1;
		lem->max[1] = 1;
	}
}

void		move_lem(t_exil *tab, t_ltree **path, t_lem *lem)
{
	print_lem(tab->lem, path[tab->ind + 1]->room->name, 0);
	if (path[tab->ind + 1]->room == lem->end)
	{
		tab->ind = -1;
		return ;
	}
	tab->ind++;
}
