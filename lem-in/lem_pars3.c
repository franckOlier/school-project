/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_pars3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/22 10:56:26 by folier            #+#    #+#             */
/*   Updated: 2014/03/22 09:59:07 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#define CMP ft_strcmp
#define DEST tmp1->dest
#define OR tmp1->ori

int		chk_nm(char *name, t_lem *lem, int *op)
{
	t_node	*tmp;

	tmp = lem->room;
	if (*name == '#' || *name == 'L')
	{
		*op = 7;
		lem->err = name;
		return (0);
	}
	if (tmp == NULL)
		return (1);
	while (tmp->next != NULL)
	{
		if (ft_strcmp(tmp->name, name) == 0)
			return (0);
		tmp = tmp->next;
	}
	if (ft_strcmp(tmp->name, name) == 0)
		return (0);
	return (1);
}

int		check_valide_lem(t_lem *lem)
{
	if (lem->lem == 0)
	{
		lem->err = ft_strdup("relances avec des lem-in ca marchera mieux...");
		return (1);
	}
	if (lem->room == NULL)
	{
		lem->err = ft_strdup("Room coord_x coord_y");
		return (1);
	}
	if (lem->start == NULL)
	{
		lem->err = ft_strdup("##start");
		return (1);
	}
	if (lem->end == NULL)
	{
		lem->err = ft_strdup("##end");
		return (1);
	}
	return (0);
}
