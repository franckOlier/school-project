/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_ltree2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: folier <folier@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/23 15:33:30 by folier            #+#    #+#             */
/*   Updated: 2014/02/23 21:11:57 by folier           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		queue_free(t_list *queue)
{
	(void)queue;
}

void		maj_statut_room(t_lem *lem, t_ltree **path)
{
	int		i;
	t_node	*tmp;

	i = 0;
	tmp = lem->room;
	while (path[i])
		path[i++]->room->use = TRUE;
	while (tmp)
	{
		tmp->check = FALSE;
		tmp = tmp->next;
	}
}
